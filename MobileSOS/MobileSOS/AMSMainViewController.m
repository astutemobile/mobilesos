//
//  AMSMainViewController.m
//  MobileSOS
//
//  Created by Rob Pungello on 5/3/16.
//  Copyright © 2017 Astute Solutions, Inc. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#import "AMSMainViewController.h"
#import "AMSConfiguration.h"

@interface AMSMainViewController ()

@end

@implementation AMSMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    double autoPopTimer = [AMSConfiguration sharedConfig].autoPopTimer;
    
    if(autoPopTimer > 0) {
        autoPop = [NSTimer scheduledTimerWithTimeInterval: autoPopTimer
                                         target: self
                                         selector: @selector(doAutoPop:)
                                         userInfo: nil
                                         repeats: NO];
    }
}

- (IBAction) popOut: (id) sender {
    [autoPop invalidate];
    [self presentSOSView];
    
}

- (void) doAutoPop: (NSTimer *) timer {
    NSString *prompTitle = [AMSConfiguration sharedConfig].autoPopTitle;
    NSString *promptText = [AMSConfiguration sharedConfig].autoPopText;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle: prompTitle
                                                  message: promptText
                                                  preferredStyle: UIAlertControllerStyleActionSheet];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self presentSOSView];
    }];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
    }];
    
    [alert addAction: yesAction];
    [alert addAction: noAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) presentSOSView {
    NSString *storyboardId = @"SOS";//[AMSConfiguration sharedConfig].storyboardId;
    UIStoryboard *sb = [UIStoryboard storyboardWithName: storyboardId bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier: @"SOS"];
    vc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController: vc animated: YES completion: NULL];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
