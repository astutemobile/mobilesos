//
//  AMSConfiguration.h
//  MobileSOS
//
//  Created by Rob Pungello on 4/13/16.
//  Copyright © 2017 Astute Solutions, Inc. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKIt.h>

typedef enum amsSettings {
    kAMSInteractionBusUrl = 0,
    kAMSPromptTitle,
    kAMSPromptText,
    kAMSAutoPopTimer,
    kAMSAutoPopText,
    kAMSRecordHistory,
    kAMSRetriesToAttempt
} AMSSetting;

@interface AMSConfiguration : NSObject

@property (strong, nonatomic) NSString *interactionBusUrl;
@property (strong, nonatomic) NSString *stunServer;
@property (strong, nonatomic) NSString *stunUsername;
@property (strong, nonatomic) NSString *stunCredential;
@property (strong, nonatomic) NSString *turnServer;
@property (strong, nonatomic) NSString *promptTitle;
@property (strong, nonatomic) NSString *promptText;
@property (nonatomic, assign) int autoPopTimer;
@property (strong, nonatomic) NSString *autoPopTitle;
@property (strong, nonatomic) NSString *autoPopText;
@property (nonatomic, assign) BOOL debugMode;
@property (nonatomic, assign) BOOL recordHistory;
@property (strong, nonatomic) NSDictionary *historyTextOptions;
@property (nonatomic, assign) BOOL videoCallEnabled;
@property (nonatomic, assign) BOOL audioCallEnabled;
@property (nonatomic, assign) BOOL chatEnabled;
@property (nonatomic, assign) int retriesToAttempt;
@property (strong, nonatomic) NSString *casePopData;


+ (AMSConfiguration *) sharedConfig;

- (bool) configurationExists;
- (void) setInteractionBusUrl: (NSString *) url;
- (void) setStunServer: (NSString *) stun;
- (void) setTurnServer: (NSString *) turn;
- (void) setPromptTitle: (NSString *) title;
- (void) setPromptText: (NSString *) text;
- (void) setAutoPopTimer: (int) timer;
- (void) setAutoPopTitle: (NSString *) autoTitle;
- (void) setAutoPopText: (NSString *) autoText;
- (void) setDebugMode: (BOOL) debug;
- (void) setVideoCallEnabled: (BOOL) enabled;
- (void) setAudioCallEnabled: (BOOL) enabled;
- (void) setChatEnabled: (BOOL) enabled;
- (void) setRecordHistory: (BOOL) recordHistory;
- (void) setHistoryTextOptions:(NSDictionary *) historyTextOptions;
- (void) setStoryboardId:(NSString *) storyboardId;
- (void) setRetriesToAttempt: (int) retries;
- (void) setCasePopData: (NSString *) casePopData;


@end
