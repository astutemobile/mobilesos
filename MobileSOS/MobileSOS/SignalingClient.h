//
//  SignalingClient.h
//  DeltaSkyProfessor
//
//  Created by Rob Pungello on 12/12/17.
//  Copyright © 2017 AstuteSolutions. All rights reserved.
//

#import "SignalR.h"
#import "SRConnectionDelegate.h"
#import "AMSConfiguration.h"
#import "AMSCasePop.h"
#import "Reachability.h"

@protocol SignalingClientDelegate <NSObject>
- (void) signalingConnectionDidOpen:(id <SRConnectionInterface>)connection;
- (void) signalingConnectionLoginComplete;
- (void) signalingConnection:(id <SRConnectionInterface>)connection didReceiveData:(id)data;
- (void) signalingConnectionDidReceiveSessionDetails: (NSDictionary *) chatSession;
- (void) signalingConnectionDidReceiveChatMessage: (NSString *) chatMessage;
- (void) signalingConnectionDidFail: (NSString *) errorMessage;
- (void) signalingConnectionDidReceiveDisconnect;
- (void) signalingConnectionDidReceiveNoAgentsAvailable;
@end


@interface SignalingClient : NSObject <SRConnectionDelegate>

@property (nonatomic, strong, readwrite) SRHubConnection *hubConnection;
@property (nonatomic, strong, readwrite) SRHubProxy *customerHub;
@property (nonatomic, weak) id<SignalingClientDelegate> delegate;
@property (nonatomic, strong, readwrite) NSMutableDictionary *chatSession;
@property (nonatomic, strong, readwrite) AMSCasePop *casePopData;
@property (nonatomic) Reachability *hostReachability;
@property (nonatomic) Reachability *internetReachability;
@property (nonatomic) Reachability *wifiReachability;

- (instancetype) init;
- (instancetype) initWithDelegate: (id<SignalingClientDelegate>) delegate;

// Establishes the connection with the IB
- (void) startSignaling;
- (void) stopSignaling;
- (void) signalingServerChanged;
- (void) connectToVideo;
- (void) connectToChat;
- (void) endCall;
- (void) sendCustomerLogin;
- (void) sendChatMessage: (NSString *) message;
- (void) cleanup;
- (bool) isConnected;

@end

