//
//  JSONUtils.m
//  AstuteDemo
//
//  Created by Rob Pungello on 2/8/18.
//  Copyright © 2018 AstuteSolutions. All rights reserved.
//

#import "JSONUtils.h"

@implementation JSONUtils

+ (NSString *) serialize: (NSDictionary *) dict {
    NSError *error = nil;
    NSData *json;
    
    if ([NSJSONSerialization isValidJSONObject:dict])
    {
        json = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&error];
        
        if (json != nil && error == nil) {
            return [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
        } else {
            NSLog(@">>>>> SignalingClient ERROR:  serialization error -> %@", [error description]);
        }
    }
    
    return @"";
}

+ (id) deserialize: (NSString *) jsonString {
    NSError *jsonError;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    id object = [NSJSONSerialization JSONObjectWithData: jsonData
                                                options: kNilOptions
                                                  error: &jsonError];
    
    if(jsonError) {
        NSLog(@">>>>> ERROR: SignalingClient deserialization error -> %@", [jsonError description]);
        return nil;
    }
    
    return object;
}

@end
