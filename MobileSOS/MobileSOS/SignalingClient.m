//
//  SignalingClient.m
//  DeltaSkyProfessor
//
//  Created by Rob Pungello on 12/12/17.
//  Copyright © 2017 AstuteSolutions. All rights reserved.
//

#import "SignalingClient.h"

@implementation SignalingClient
    SRHubConnection *_hubConnection;
    SRHubProxy *_customerHub;
    NSMutableDictionary *chatSession;
    AMSCasePop *casePopData;

- (instancetype) init {
    if (self = [super init]) {
        [self configure];
    }
    return self;
}

- (instancetype) initWithDelegate: (id<SignalingClientDelegate>) delegate {
    if (self = [super init]) {
        _delegate = delegate;
        [self configure];
    }
    return self;
}

- (void) configure {
    NSString *iBus = [AMSConfiguration sharedConfig].interactionBusUrl;
    
    @try {
        if(iBus == nil)  {
            NSException *e = [NSException
                              exceptionWithName:@"InteractionBusUrlUndefined"
                              reason:@"You must set an Interaction Bus URL"
                              userInfo:nil];
            @throw e;
        }
    }
    @catch(NSException *e) {
        [_delegate signalingConnectionDidFail: [e reason]];
        return;
    }
    
    _casePopData = [AMSCasePop sharedCasePop];
    
    //NSLog(@"signaling client connection state = %i", _hubConnection.state);
    
    if(_hubConnection.state == 1) {
        [_hubConnection disconnect];
    }
    
    _hubConnection = nil;
    _hubConnection = [SRHubConnection connectionWithURLString: iBus];
    [_hubConnection setDelegate: self];

    _customerHub = [_hubConnection createHubProxy:@"CustomerHub"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name: kReachabilityChangedNotification object:nil];
    
    // Monitor reachability of the interaction bus
    [self startReachabilityNotifications: iBus];
    
    _chatSession = [[NSMutableDictionary alloc] init];
    
    // set our case pop data
    [self createCasePopData];
}

- (void) reachabilityChanged: (NSNotification *) note {
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self checkReachability: curReach];
}

- (void)checkReachability: (Reachability *) reachability {
    if (reachability == self.hostReachability)
    {
        BOOL connectionRequired = [reachability connectionRequired];
        
        NSString* messageText = @"";
        
        if (connectionRequired) {
            messageText = NSLocalizedString(@"Cellular data network is available.\nInternet traffic will be routed through it after a connection is established.", @"Reachability text if a connection is required");
        } else {
            messageText = NSLocalizedString(@"Cellular data network is active.\nInternet traffic will be routed through it.", @"Reachability text if a connection is not required");
        }
        
        //NSLog(@">>>>> SignalingClient %@", messageText);
    }
    
    if (reachability == self.internetReachability) {
        [self configureMessageForReachability: reachability];
    }
    
    if (reachability == self.wifiReachability)
    {
        [self configureMessageForReachability: reachability];
    }
}

- (void)configureMessageForReachability:(Reachability *)reachability {
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    BOOL connectionRequired = [reachability connectionRequired];
    NSString* statusString = @"";
    
    switch (netStatus)
    {
        case NotReachable:        {
            statusString = @"Access Not Available";
            connectionRequired = NO;
            break;
        }
            
        case ReachableViaWWAN:        {
            statusString = NSLocalizedString(@"Reachable WWAN", @"");
            break;
        }
        case ReachableViaWiFi:        {
            statusString= NSLocalizedString(@"Reachable WiFi", @"");
            break;
        }
    }
    
    if (connectionRequired)
    {
        NSString *connectionRequiredFormatString = NSLocalizedString(@"%@, Connection Required", @"Concatenation of status string with connection requirement");
        statusString= [NSString stringWithFormat:connectionRequiredFormatString, statusString];
    }
    
    if([AMSConfiguration sharedConfig].debugMode) {
        //NSLog(@">>>>> SignalingClient reachability status -> %@", statusString);
    }
}

- (void) handleException: (NSException *) e {
    NSLog(@"An Interaction Bus URL must be set before connecting..");
}

- (void) signalingServerChanged {
    [self configure];
    [self startSignaling];
}

- (void) startSignaling {
    if(_hubConnection == nil) {
        //NSLog(@">>>>> SignalingClient being reconfigured");
        [self configure];
    }
    
    // state 3 = disconnected
    //NSLog(@"signaling client connection state = %i", _hubConnection.state);
    if(_hubConnection.state == 3) {
        [_hubConnection start];
    }
}

- (void) stopSignaling {
    [self cleanup];
}

- (void) cleanup {
    //NSLog(@">>> SignalingClient cleaning up");
    [_hubConnection stop];
    //_hub = nil;
    //_connection = nil;
    
    [self stopReachabilityNotifications];
    
}

- (void) startReachabilityNotifications: (NSString *) url {
    self.hostReachability = [Reachability reachabilityWithHostName: url];
    [self.hostReachability startNotifier];
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    
    self.wifiReachability = [Reachability reachabilityForLocalWiFi];
    [self.wifiReachability startNotifier];
}

- (void) stopReachabilityNotifications {
    [self.hostReachability stopNotifier];
    [self.internetReachability stopNotifier];
    [self.wifiReachability stopNotifier];
}

- (void) dealloc {
    [self cleanup];
}

#pragma mark Messages
- (NSMutableDictionary *) getRequireAgent {
    NSDictionary *ra = @{@"CustomerName" : @"1",
                         @"Customer" : @"WebRTCProvider",
                         @"Message" : @"CUSTOMER",
                         @"Provider" : @"2"};
    return [[NSMutableDictionary alloc] initWithDictionary: ra];
}

- (void) sendCustomerLogin {
    NSDictionary *customerLogin = @{@"ProviderList" : @"1",
                               @"UserName" : @"CUSTOMER"};
    
    NSArray *args = [NSArray arrayWithObject: customerLogin];
    [_customerHub invoke: @"CustomerLogin" withArgs: args];
}

- (void) connectToVideo {
    NSMutableDictionary *ra = [self getRequireAgent];
    [ra setObject: @"3" forKey: @"Provider"];
    [ra setObject: @"Video" forKey: @"CommunicateOn"];
    
    NSArray *args = [NSArray arrayWithObject: ra];
    [_customerHub invoke: @"RequireAgentJson" withArgs: args];
}

- (void) connectToChat {
    NSMutableDictionary *ra = [self getRequireAgent];
    [ra setObject: @"1" forKey: @"Provider"];
    [ra setObject: @"Chat" forKey: @"CommunicateOn"];
    
    NSArray *args = [NSArray arrayWithObject: ra];
    [_customerHub invoke: @"RequireAgentJson" withArgs: args];
}

- (void) endCall {
    if (_chatSession != nil && [_chatSession objectForKey: @"InteractionID"] != nil) {
        NSDictionary *customerDrops = @{@"InteractionID": [_chatSession objectForKey: @"InteractionID"]};
        NSArray *args = [NSArray arrayWithObject: customerDrops];
        [_customerHub invoke: @"CustomerDropsChat" withArgs: args];
        
        [_delegate signalingConnectionDidReceiveDisconnect]; 
    }
    
    [self cleanupCall];
}

- (void) cleanupCall {
    _chatSession = nil;
}

// vars received from chatSession object
- (void) sendChatMessage: (NSString *) message {
    NSMutableDictionary *sendChatMessage = [[NSMutableDictionary alloc] initWithCapacity: 11];
    [sendChatMessage setValue: @"ChatMessage" forKey: @"RequestType"];
    [sendChatMessage setValue: [_chatSession objectForKey: @"InteractionID"] forKey: @"InteractionID"];
    [sendChatMessage setValue: @"2" forKey: @"Provider"];
    [sendChatMessage setValue: [chatSession objectForKey: @"ConnectionId"] forKey: @"ConnectionId"];
    [sendChatMessage setValue: [chatSession objectForKey: @"AgentName"] forKey: @"AgentInfo"];
    [sendChatMessage setValue: @"CustomerName" forKey: @"Name"];
    [sendChatMessage setValue: @"" forKey: @"PhoneNumber"];
    [sendChatMessage setValue: message forKey: @"Message"];
    [sendChatMessage setValue: @"0" forKey: @"CaseId"];
    [sendChatMessage setValue: [self getCurrentDateString] forKey: @"DateTimeSent"];
    [sendChatMessage setValue: @"2" forKey: @"State"];
    
   //NSLog(@"built chat message request -> %@", sendChatMessage);
    
    NSArray *args = [NSArray arrayWithObject: sendChatMessage];
    [_customerHub invoke: @"SendChatMessage" withArgs: args];
}

- (NSString *) getCurrentDateString {
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *now = [dateFormatter stringFromDate:[NSDate date]];
    return [NSString stringWithString: now];
}

- (void) sendCasePop {
    NSString *companyID = @"";
    NSDictionary *casePop = @{@"InteractionID" : [_chatSession objectForKey: @"InteractionID"],
                              @"CompanyID" : companyID,
                              @"CaseData" : [self serialize: @{@"CaseData" : [_casePopData getCasePopData]}]};
    
    NSArray *args = [NSArray arrayWithObject: casePop];
    [_customerHub invoke: @"ChatCasePop" withArgs: args];
}

- (void) createCasePopData {
    NSDictionary *locationText = @{@"text_type_code" : @"VERBATIM",
                                   @"case_text" : @"Starting SOS Video" };
    [_casePopData addText: locationText];
    
    NSDictionary *bCode1 = @{@"b05_code" : @"SOSVIDEO"};
    
    [_casePopData addCode: bCode1];
}

- (bool) isConnected {
    return connected;
}

#pragma mark - Data parsing
- (NSString *) serialize: (NSDictionary *) dict {
    NSError *error = nil;
    NSData *json;
    
    if ([NSJSONSerialization isValidJSONObject:dict])
    {
        json = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&error];
        
        if (json != nil && error == nil) {
            return [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
        } else {
            NSLog(@">>>>> SignalingClient ERROR:  serialization error -> %@", [error description]);
        }
    }
    
    return @"";
}

- (id) deserialize: (NSString *) jsonString {
    NSError *jsonError;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    id object = [NSJSONSerialization JSONObjectWithData: jsonData
                                                options: kNilOptions
                                                  error: &jsonError];
    
    if(jsonError) {
        NSLog(@">>>>> ERROR: SignalingCClient deserialization error -> %@", [jsonError description]);
        return nil;
    }
    
    return object;
}

- (void) parseData: (NSArray *) dataArray forMethod: (NSString *) method {
    if([method isEqualToString: @"CustomerNotification"]) {
        NSDictionary *customerNotification = [dataArray objectAtIndex: 0];
        //NSLog(@"signalingClient got customer notification -> %@", customerNotification);
        NSString *customerState = [customerNotification objectForKey: @"CustomerState"];
        switch([customerState intValue]) {
            case 0:
                [_delegate signalingConnectionLoginComplete];
                break;
            case 2:
                [_chatSession addEntriesFromDictionary: customerNotification];
                [_delegate signalingConnectionDidReceiveSessionDetails: _chatSession];
                [self sendCasePop];
                break;
            case 3:
                NSLog(@"display chat message -> no agents are available");
                [_delegate signalingConnectionDidReceiveNoAgentsAvailable];
                [self cleanupCall];
                break;
            case 4:
                [_delegate signalingConnectionDidReceiveDisconnect];
                [self endCall];
                break;
                
        }
    } else if([method isEqualToString: @"ChatMessage"]) {
        NSDictionary *chatMessage = [dataArray objectAtIndex: 0];
        //NSLog(@"signalingClient got chat message -> %@", chatMessage);
        [_delegate signalingConnectionDidReceiveChatMessage: [chatMessage objectForKey: @"Message"]];
    }  else {
        NSLog(@">>>>> unrecognized data received from Interaction Bus!");
    }
}


#pragma mark - SRConnectionDelegate methods
- (void)SRConnectionDidOpen:(id <SRConnectionInterface>)connection {
    if([AMSConfiguration sharedConfig].debugMode) {
        NSLog(@">>>> SignalingClient connection opened, sending login credentials");
    }
    
    [_delegate signalingConnectionDidOpen: connection];
}

- (void)SRConnectionWillReconnect:(id <SRConnectionInterface>)connection {
    if([AMSConfiguration sharedConfig].debugMode) {
        NSLog(@">>>>> SignalingClient connection will reconnect");
    }
}

- (void)SRConnectionDidReconnect:(id <SRConnectionInterface>)connection {
    if([AMSConfiguration sharedConfig].debugMode) {
        NSLog(@">>>>> SignalingClient connection did reconnect, do something!!");
    }
}

- (void)SRConnection:(id <SRConnectionInterface>)connection didReceiveData:(id)data {
    NSLog(@"signaling client received data -> %@", data);
    NSString *method = [data objectForKey: @"M"];
    
    [self parseData: [data objectForKey: @"A"] forMethod: method];
}

- (void)SRConnectionDidClose:(id <SRConnectionInterface>)connection {
    if([AMSConfiguration sharedConfig].debugMode) {
        NSLog(@">>>>> SignalingClient connection closed");
    }
    
    _customerHub = nil;
    _hubConnection = nil;
    
    [self stopReachabilityNotifications];
    
}

- (void)SRConnection:(id <SRConnectionInterface>)connection didReceiveError:(NSError *) error {
    NSLog(@">>>>> SignalingClient connection error - %@", [error description]);
    [_delegate signalingConnectionDidFail: [error localizedDescription]];
    
    [self startSignaling];
}

- (void)SRConnection:(id <SRConnectionInterface>)connection didChangeState:(connectionState)oldState newState:(connectionState)newState {
    if([AMSConfiguration sharedConfig].debugMode) {
        NSString *newStateStr = @"unknown";
        switch (newState) {
            case 0:
                newStateStr = @"connecting";
                break;
            case 1:
                newStateStr = @"connected";
                break;
            case 2:
                newStateStr = @"reconnecting";
                break;
            case 3:
                newStateStr = @"disconnected";
                break;
            default:
                break;
        }
        
        NSLog(@">>>>> SignalingClient connection changed state to %@", newStateStr);
    }
}

- (void)SRConnectionDidSlow:(id <SRConnectionInterface>)connection {
    if([AMSConfiguration sharedConfig].debugMode) {
        NSLog(@">>>>> SignalingClient connection has slowed");
    }
}

@end

