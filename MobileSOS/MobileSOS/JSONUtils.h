//
//  JSONUtils.h
//  AstuteDemo
//
//  Created by Rob Pungello on 2/8/18.
//  Copyright © 2018 AstuteSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONUtils : NSObject

+ (NSString *) serialize: (NSDictionary *) dict;
+ (id) deserialize: (NSString *) jsonString;


@end
