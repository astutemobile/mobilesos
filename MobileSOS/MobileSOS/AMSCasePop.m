//
//  AMSCasePopData.m
//  MobileSOS
//
//  Created by Rob Pungello on 4/7/16.
//  Copyright © 2017 Astute Solutions, Inc. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#import "AMSCasePop.h"
#import "AMSConfiguration.h"
#import "AMSDataHandler.h"

@implementation AMSCasePop

+ (AMSCasePop *) sharedCasePop {
    static AMSCasePop* _sharedCasePop = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedCasePop = [[self alloc] init];
    });
    
    return _sharedCasePop;
}


- (instancetype) init {
    if (self = [super init]) {
        [self initialize];
    }
    return self;
}

- (void) initialize {
    _codes = [NSMutableArray array];
    _caseIds = [NSMutableArray array];
    _addresses = [NSMutableArray array];
    _issues = [NSMutableArray array];
    _texts = [NSMutableArray array];
    _esps = [NSMutableArray array];
    
    config = [AMSConfiguration sharedConfig];
}

- (void) loadFromJsonString:(NSString *)jsonString {
    if(jsonString == nil) {
        return;
    }
    
    
    AMSDataHandler *dataHandler = [[AMSDataHandler alloc] init];
    NSDictionary *jsonDict = [dataHandler deserialize: jsonString];
    
    for(NSString *key in jsonDict) {
        if([key isEqualToString: @"addresses"]) {
            _addresses = [NSMutableArray arrayWithArray: [jsonDict objectForKey: key]];
        } else if([key containsString: @"b01_"]) {
            [self addCode: @{key : [jsonDict objectForKey: key]}];
        } else if([key isEqualToString: @"case_id"]) {
            [self addCaseId: @{key : [jsonDict objectForKey: key]}];
        } else if([key isEqualToString: @"esp"]) {
            _esps = [NSMutableArray arrayWithArray: [jsonDict objectForKey: key]];
        } else if([key isEqualToString: @"issues"]) {
            _issues = [NSMutableArray arrayWithArray: [jsonDict objectForKey: key]];
        } else if([key isEqualToString: @"texts"]) {
            _texts = [NSMutableArray arrayWithArray: [jsonDict objectForKey: key]];
        }
    }
}

- (NSArray *) getCodeFields {
    NSMutableArray *bCodeFields = [NSMutableArray array];
    for(int i = 1; i < 100; i++) {
        NSString *code;
        if(i < 10) {
            code = [NSString stringWithFormat: @"b0%d_code", i];
        } else {
            code = [NSString stringWithFormat: @"b%d_code", i];
        }
        
        [bCodeFields addObject: code];
    }
    
    return bCodeFields;
}

- (NSArray *) getCaseIdFields {
    return @[@"case_id"];
}

- (NSArray *) getAddressFields {
    NSMutableArray * addrFields = [NSMutableArray arrayWithArray: @[@"given_names",@"last_name",@"middle_initial",@"name_title",@"suffix",@"address1",@"address2",@"address3",@"city",@"postal_code",@"state",@"country",@"email2",@"email3",@"company_name",@"address_type_code"]];
    
    for(int i = 1; i < 100; i++) {
        NSString *code;
        if(i < 10) {
            code = [NSString stringWithFormat: @"a0%d_code", i];
        } else {
            code = [NSString stringWithFormat: @"a%d_code", i];
        }
        
        [addrFields addObject: code];
    }
    
    return addrFields;
}

- (NSArray *) getIssueFields {
    NSMutableArray *issueFields = [NSMutableArray array];
    for(int i = 1; i < 100; i++) {
        NSString *code;
        if(i < 10) {
            code = [NSString stringWithFormat: @"c0%d_code", i];
        } else {
            code = [NSString stringWithFormat: @"c%d_code", i];
        }
        
        [issueFields addObject: code];
    }
    
    return issueFields;
}

- (NSArray *) getTextFields {
    return @[@"text_type_code", @"case_text"];
}

- (NSArray *) getEspFields {
    return @[@"url", @"title", @"text"];
}

- (void) addCode: (NSDictionary *) code {
    [_codes addObject: code];
}

- (void) addCaseId: (NSDictionary *) caseId {
    [_caseIds addObject: caseId];
}

- (void) addAddress: (NSDictionary *) address {
    NSMutableDictionary *mutableAddress = [NSMutableDictionary dictionaryWithDictionary: address];
    [_addresses addObject: mutableAddress];
}

- (void) addFieldToAddress: (NSDictionary *) address {
    if(address == nil || address.count == 0) {
        return;
    }
    
    if(_addresses.count == 0) {
        [self addAddress: address];
    } else {
        for(NSString *key in address) {
            NSMutableDictionary *updatedAddress = [_addresses objectAtIndex: 0];
            [updatedAddress setObject: [address objectForKey: key] forKey: key];
            [_addresses setObject: updatedAddress atIndexedSubscript: 0];
        }
    }
}
        
// TODO: update validation?
- (void) addIssue: (NSDictionary *) issue {
    [_issues addObject: issue];
}

// TODO: let's compare the text.. if there's one of the same name, replace it
// else we can add a new one
- (void) addText: (NSDictionary *) text {
    /*
    for(NSDictionary *dict in _texts) {
        NSString *key = @"Starting SOS Video";
        if([dict objectForKey: @"case_text"]) {
            [key isEqualToString: [dict objectForKey: @"case_text"]] {
                
            }
        }
    }
    */
    [_texts addObject: text];
}

// transcript is a text with @"Self Service" as the text_type_code
/*
 NSDictionary *transcript = @{@"text_type_code" : @"Self Service",
 @"case_text" : conversation};
 */
- (void) replaceTranscript: (NSDictionary *) transcript {
    if(_texts.count == 0) {
        [self addText: transcript];
        return;
    }
    
    for(int i = 0; i < _texts.count; i++) {
        NSDictionary *dict = [_texts objectAtIndex: i];
        NSString *textTypeCode = [dict objectForKey: @"text_type_code"];
        if(textTypeCode) {
            if([textTypeCode isEqualToString: @"Self Service"]) {
                [_texts setObject: transcript atIndexedSubscript: i];
            }
        }
    }
}

- (void) addEsp: (NSDictionary *) esp {
    [_esps addObject: esp];
}

- (void) addUserEnteredName: (NSString *) name {
    if(name == nil || [name isEqualToString: @""]) {
        return;
    }
    
    NSArray *components = [name componentsSeparatedByString: @" "];
    NSMutableArray *names = [NSMutableArray arrayWithArray: components];
    NSMutableDictionary *addr = [NSMutableDictionary dictionary];
    if(names.count == 1) {
        [addr setObject: names[0] forKey: @"given_names"];
    } else if(names.count > 1 ) {
        NSString *last = [names objectAtIndex: names.count-1];
        [addr setObject: last forKey: @"last_name"];
        [names removeLastObject];
        [addr setObject: [names componentsJoinedByString: @" "] forKey: @"given_names"];
    }
    
    [self addFieldToAddress: addr];
}

- (void) addHistoryText: (NSString *) historyStr {
    
    NSDictionary *options = config.historyTextOptions;
    NSString *textTypeCode = nil;
    if(options == nil) {
        textTypeCode = @"History";
    } else {
        textTypeCode = [options objectForKey: @"text_type_code"];
    }
    
    NSDictionary *historyItem = @{@"text_type_code" : textTypeCode,
                                  @"case_text" : historyStr};
    
    [self addText: historyItem];
}

- (void) clearCasePopData {
    [self initialize];
}

#pragma mark - Data validation methods
- (bool) isCodeFieldValid: (NSString *) field {
    return [[self getCodeFields] indexOfObject: field] != NSNotFound;
}

- (bool) isAddressFieldValid: (NSString *) field {
    return [[self getAddressFields] indexOfObject: field] != NSNotFound;
}

- (bool) isIssueFieldValid: (NSString *) field {
    return [[self getIssueFields] indexOfObject: field] != NSNotFound;
}

- (bool) isTextFieldValid: (NSString *) field {
    return [[self getTextFields] indexOfObject: field] != NSNotFound;
}

- (bool) isEspFieldValid: (NSString *) field {
    return [[self getEspFields] indexOfObject: field] != NSNotFound;
}

#pragma mark - Data transform methods
- (NSDictionary *) getTransformedCasePopData: (NSDictionary *) casePopData {
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    for (id key in casePopData) {
        if ([key isEqualToString: @"issues"]) {
            NSArray *itemData = [self transformCasePopDataItems: [casePopData objectForKey: @"issues"]];
            NSDictionary *issue = @{@"Issue" : itemData};
            [result setObject: issue forKey: @"IssueList"];
        } else if ([key isEqualToString: @"texts"]) {
            NSArray *textData = [self transformCasePopDataItems: [casePopData objectForKey: @"texts"]];
            NSDictionary *text = @{@"CaseText" : textData};
            [result setObject: text forKey: @"CaseTextList"];
        } else if ([key isEqualToString: @"attachments"]) {
            NSArray *attachmentData = [self transformCasePopDataItems: [casePopData objectForKey: @"attachments"]];
            [result setObject: attachmentData forKey: @"CaseAttachmentList"];
        } else if ([key isEqualToString: @"addresses"]) {
            NSArray *addressData = [self transformCasePopDataItems: [casePopData objectForKey: @"addresses"]];
            NSDictionary *address = @{@"Address" : addressData};
            [result setObject: address forKey: @"AddressList"];
        } else {
            NSString *newKey = [NSString stringWithFormat: @"_$%@", key];
            [result setObject: [casePopData objectForKey: key] forKey: newKey];
        }
    }
    return result;
};

- (NSDictionary *) transformCasePopDataObject: (NSDictionary *) original {
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    for (id key in original) {
        NSString *newKey = [NSString stringWithFormat: @"_$%@", key];
        [result setObject: [original objectForKey: key] forKey: newKey];
    }
    
    return result;
}

- (NSArray *) transformCasePopDataItems: (NSArray *) items {
    NSMutableArray *result = [NSMutableArray array];
    for(id object in items) {
        [result addObject: [self transformCasePopDataObject: object]];
    }
    
    return result;
}

- (NSDictionary *) getCasePopDataForPersisting {
    NSMutableDictionary *cpd = [[NSMutableDictionary alloc] init];
    NSDictionary *partialData = @{@"addresses" : _addresses,
                                  @"issues" : _issues,
                                  @"texts" : _texts,
                                  @"esp" : _esps};
    
    
    [cpd addEntriesFromDictionary: partialData];
    
    for(id object in _codes) {
        [cpd addEntriesFromDictionary: object];
    }
    
    for(id object in _caseIds) {
        [cpd addEntriesFromDictionary: object];
    }
    
    return cpd;
}

- (NSDictionary *) getCasePopData {
    NSMutableDictionary *casePopData = [[NSMutableDictionary alloc] init];
    NSDictionary *partialData = @{@"addresses" : _addresses,
                                  @"issues" : _issues,
                                  @"texts" : _texts,
                                  @"esp" : _esps};
    
    
    [casePopData addEntriesFromDictionary: partialData];
    
    for(id object in _codes) {
        [casePopData addEntriesFromDictionary: object];
    }
    
    for(id object in _caseIds) {
        [casePopData addEntriesFromDictionary: object];
    }
    
    return [self getTransformedCasePopData: casePopData];
}

#pragma mark - Helpers
- (void) printJsonDictionary: (NSDictionary *) jsonDict {
    NSEnumerator *enumerator = [jsonDict keyEnumerator];
    id key;
    while ((key = [enumerator nextObject])) {
        NSDictionary *tmp = [jsonDict objectForKey: key];
        NSLog(@"%@ = %@", key, tmp);
    }
}

- (void) createCasePopSample {
    NSDictionary *addr1 = @{@"given_names" : @"John",
                            @"last_name" : @"Smith",
                            @"email2" : @"john@example.com",
                            @"address1" : @"386 Astute Blvd.",
                            @"city" : @"Columbus",
                            @"state" : @"OH",
                            @"postal_code" : @"43231"};
    [self addAddress: addr1];
    
    NSDictionary *text1 = @{@"text_type_code" : @"verbatim",
                            @"case_text" : @"Greetings from Mobile SOS!"};
    [self addText: text1];
    
    NSDictionary *issue1 = @{@"c05_code" : @"General Inquiry"};
    [self addIssue: issue1];
    
    NSDictionary *esp1 = @{@"url": @"http://www.astutesolutions.com/",
                           @"title" : @"Astute Solutions",
                           @"text" : @"The makers of the SOS widget!"};
    NSDictionary *esp2 = @{@"url" : @"http://www.google.com/",
                           @"title" : @"Google",
                           @"text": @"Search Overlord"};
    [self addEsp: esp1];
    [self addEsp: esp2];
}

@end

