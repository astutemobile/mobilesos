//
//  AMSConfiguration.m
//  MobileSOS
//
//  Created by Rob Pungello on 4/13/16.
//  Copyright © 2017 Astute Solutions, Inc. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#import "AMSConfiguration.h"

static NSString * const kInteractionBusUrl = @"interactionBusUrl";
static NSString * const kPromptTitle = @"promptTitle";
static NSString * const kPromptText = @"promptText";
static NSString * const kAutoPopTimer = @"autoPopTimer";
static NSString * const kAutoPopText = @"autoPopText";
static NSString * const kAutoPopTitle = @"autoPopTitle";
static NSString * const kDebugMode = @"debugMode";
static NSString * const kRecordHistory = @"recordHistory";
static NSString * const kHistoryTextOptions = @"historyTextOptions";
static NSString * const kVideoCallEnabled = @"videoCallEnabled";
static NSString * const kAudioCallEnabled = @"audioCallEnabled";
static NSString * const kChatEnabled = @"chatEnabled";
static NSString * const kRetriesToAttempt = @"retriesToAttempt";
static NSString * const kCasePopData = @"casePopData";
static NSString * const kTextTypeCode = @"text_type_code";

@implementation AMSConfiguration

+ (AMSConfiguration *) sharedConfig {
    static AMSConfiguration *_sharedConfig = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedConfig = [[self alloc] init];
    });
    
    return _sharedConfig;
}

- (instancetype)init {
    if (self = [super init]) {
        [self loadConfiguration];
    }
    return self;
}

- (void) initializeDefaults {
    _interactionBusUrl = nil;
    _promptTitle = @"Do you need help?";
    _promptText = @"Click OK to connect with an agent..";
    _autoPopTimer = 10;
    _autoPopTitle = @"Hello There!";
    _autoPopText = @"Need help?";
    _debugMode = YES;
    _recordHistory = NO;
    _historyTextOptions = [NSDictionary dictionaryWithObject: @"History" forKey: kTextTypeCode];
    _videoCallEnabled = YES;
    _audioCallEnabled = YES;
    _chatEnabled = YES;
    
    _retriesToAttempt = 3;
    
    _stunServer = nil;
    _stunUsername = nil;
    _stunCredential = nil;
    _turnServer = nil;
    
    _casePopData = @"";
}

- (void) saveConfiguration {
    [[NSUserDefaults standardUserDefaults] setObject: _interactionBusUrl forKey: kInteractionBusUrl];
    [[NSUserDefaults standardUserDefaults] setObject: _promptTitle forKey: kPromptTitle];
    [[NSUserDefaults standardUserDefaults] setObject: _promptText forKey: kPromptText];
    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithInt: _autoPopTimer] forKey: kAutoPopTimer];
    [[NSUserDefaults standardUserDefaults] setObject: _autoPopTitle forKey: kAutoPopTitle];
    [[NSUserDefaults standardUserDefaults] setObject: _autoPopText forKey: kAutoPopText];
    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithBool: _debugMode] forKey: kDebugMode];
    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithBool: _recordHistory] forKey: kRecordHistory];
    [[NSUserDefaults standardUserDefaults] setObject: _historyTextOptions forKey: kHistoryTextOptions];
    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithBool: _videoCallEnabled] forKey: kVideoCallEnabled];
    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithBool: _audioCallEnabled] forKey: kAudioCallEnabled];
    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithBool: _chatEnabled] forKey: kChatEnabled];
    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithInt: _retriesToAttempt] forKey: kRetriesToAttempt];
    [[NSUserDefaults standardUserDefaults] setObject: _casePopData forKey: kCasePopData];

    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) loadConfiguration {
    if ([[NSUserDefaults standardUserDefaults] objectForKey: kInteractionBusUrl]) {
        _interactionBusUrl = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey: kInteractionBusUrl];
        _promptTitle = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey: kPromptTitle];
        _promptText = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey: kPromptText];
        _autoPopTimer = [(NSNumber *)[[NSUserDefaults standardUserDefaults] objectForKey: kAutoPopTimer] intValue];
        _autoPopTitle = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey: kAutoPopTitle];
        _autoPopText = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey: kAutoPopText];
        _debugMode = [(NSNumber *)[[NSUserDefaults standardUserDefaults] objectForKey: kDebugMode] boolValue];
        _recordHistory = [(NSNumber *)[[NSUserDefaults standardUserDefaults] objectForKey: kRecordHistory] boolValue];
        _historyTextOptions = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey: kHistoryTextOptions];
        _videoCallEnabled = [(NSNumber *)[[NSUserDefaults standardUserDefaults] objectForKey: kVideoCallEnabled] boolValue];
        _audioCallEnabled = [(NSNumber *)[[NSUserDefaults standardUserDefaults] objectForKey: kAudioCallEnabled] boolValue];
        _chatEnabled = [(NSNumber *)[[NSUserDefaults standardUserDefaults] objectForKey: kChatEnabled] boolValue];
        _autoPopTimer = [(NSNumber *)[[NSUserDefaults standardUserDefaults] objectForKey: kRetriesToAttempt] intValue];
        _casePopData = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey: kCasePopData];
    } else {
        [self initializeDefaults];
        [self saveConfiguration];
    }
}

- (bool) configurationExists {
    if([[NSUserDefaults standardUserDefaults] objectForKey: kInteractionBusUrl]) {
        return YES;
    }
    
    return NO;
}

- (void) setInteractionBusUrl: (NSString *) url {
    _interactionBusUrl = url;
    [[NSUserDefaults standardUserDefaults] setObject: _interactionBusUrl forKey: kInteractionBusUrl];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

// we don't persist the stun and turn servers as they come over from the IB when the connection happens
- (void) setStunServer: (NSString *) stun {
    _stunServer = stun;
}

- (void) setStunUsername: (NSString *) username {
    _stunUsername = username;
}

- (void) setStunCredential: (NSString *) credential {
    _stunCredential = credential;
}

- (void) setTurnServer: (NSString *) turn {
    _turnServer = turn;
}

- (void) setPromptTitle: (NSString *) title {
    _promptTitle = title;
    [[NSUserDefaults standardUserDefaults] setObject: _promptTitle forKey: kPromptTitle];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setPromptText: (NSString *) text {
    _promptText = text;
    [[NSUserDefaults standardUserDefaults] setObject: _promptText forKey: kPromptText];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setAutoPopTimer: (int) timer {
    _autoPopTimer = timer;
    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithInt: _autoPopTimer] forKey: kAutoPopTimer];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setAutoPopTitle: (NSString *) autoTitle {
    _autoPopTitle = autoTitle;
    [[NSUserDefaults standardUserDefaults] setObject: _autoPopTitle forKey: kAutoPopTitle];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setAutoPopText: (NSString *) autoText {
    _autoPopText = autoText;
    [[NSUserDefaults standardUserDefaults] setObject: _autoPopText forKey: kAutoPopText];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setDebugMode: (BOOL) debug {
    _debugMode = debug;
    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithBool: _debugMode] forKey: kDebugMode];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setVideoCallEnabled: (BOOL) enabled {
    _videoCallEnabled = enabled;
    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithBool: _videoCallEnabled] forKey: kVideoCallEnabled];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setAudioCallEnabled: (BOOL) enabled {
    _audioCallEnabled = enabled;
    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithBool: _audioCallEnabled] forKey: kAudioCallEnabled];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setChatEnabled: (BOOL) enabled {
    _chatEnabled = enabled;
    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithBool: _chatEnabled] forKey: kChatEnabled];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setRecordHistory: (BOOL) recordHistory {
    _recordHistory = recordHistory;
    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithBool: _recordHistory] forKey: kRecordHistory];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setHistoryTextOptions:(NSDictionary *) historyTextOptions {
    _historyTextOptions = historyTextOptions;
    [[NSUserDefaults standardUserDefaults] setObject: _historyTextOptions forKey: kHistoryTextOptions];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setHistoryTextOption: (NSString *) historyTextOption {
    [self setHistoryTextOptions: [NSDictionary dictionaryWithObject: historyTextOption forKey: kTextTypeCode]];
}

- (void) setRetriesToAttempt: (int) retries {
    _retriesToAttempt = retries;
    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithInt: _retriesToAttempt] forKey: kRetriesToAttempt];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setCasePopData: (NSString *)casePopData {
    _casePopData = casePopData;
    [[NSUserDefaults standardUserDefaults] setObject: casePopData forKey: kCasePopData];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

@end
