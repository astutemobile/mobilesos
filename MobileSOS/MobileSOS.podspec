#
#  Be sure to run `pod spec lint MobileSOS.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|
  s.name         = "MobileSOS"
  s.version      = "1.0.0"
  s.summary      = "An API to support mobile access to Astute SOS."
  #s.description  = <<-DESC  DESC
  s.homepage     = "http://www.astutesolutions.com"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "Rob Pungello" => "robpun@astutesolutions.com" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://robpun@bitbucket.org/astutemobile/mobilesos.git", :tag => "1.0.0" }
  s.source_files  = "MobileSOS", "MobileSOS/**/*.{h,m}"
  s.framework  = "SystemConfiguration"
  s.dependency "CocoaLumberjack"
  s.dependency "SignalR-ObjC", "~> 2.0"
  s.dependency "TwilioVideo", "2.0.0-preview7"
end
