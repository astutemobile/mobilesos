package com.astutesolutions.mobilesos;

/**
 * Created by Rob Pungello on 9/16/16.
 * Copyright © 2016 Astute Solutions, Inc. All rights reserved.
 */

import java.util.ArrayList;
import java.util.HashMap;

public class AMSConfiguration {
    ArrayList bCodes = new ArrayList<HashMap<String,String>>();
    ArrayList addresses = new ArrayList<HashMap<String,String>>();
    ArrayList issues = new ArrayList<HashMap<String,String>>();
    ArrayList texts = new ArrayList<HashMap<String,String>>();;
    ArrayList esps = new ArrayList<HashMap<String,String>>();;

    /* private methods */

    private ArrayList getbCodeFields() {
        ArrayList bCodeFields = new ArrayList<String>();
        for(int i = 1; i < 100; i++) {
            String code;
            if(i < 10) {
                code = String.format("b0%d_code", i);
            } else {
                code = String.format("b%d_code", i);
            }

            bCodeFields.add(code);
        }

        return bCodeFields;
    }



    HashMap<String,String> getCasePopData() {
        return null;
    }

    // TODO: not needed?
    HashMap<String,String> getCasePopData(String caller) {
        return null;
    }

    void addBCode(HashMap bCode) {
        bCodes.add(bCode);
    }

    void addAddress(HashMap address) {
        addresses.add(address);
    }

    void addIssue(HashMap issue) {
        issues.add(issue);
    }

    void addText(HashMap text) {
        texts.add(text);
    }

    void addEsp(HashMap esp) {
        esps.add(esp);
    }

    void addUserEnteredName(String name) {

    }

    void addHistoryText(String historyStr) {

    }

    void createCasePopSample() {

    }




}


