package com.astutesolutions.mobilesos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    AMSAppClient client = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        AMSConnectivityManager.sharedConnManager().setContext(this);

        client = AMSAppClient.sharedClient();

        // set up a buttons to login and logout
        final Button loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.out.println(">>>>> login button clicked");
                client.startSignaling();
            }
        });

        final Button logoutButton = (Button) findViewById(R.id.logout_button);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.out.println(">>>>> logout button clicked");
                client.stopSignaling();
            }
        });

        final Button stopButton = (Button) findViewById(R.id.stop_button);
        stopButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.out.println(">>>>> stop button clicked");
                //client.disconnect();
            }
        });


        System.out.println("Main2Activity finished onCreate");
    }

    @Override
    protected void onStop() {
        client.stopSignaling();
        super.onStop();
    }
}