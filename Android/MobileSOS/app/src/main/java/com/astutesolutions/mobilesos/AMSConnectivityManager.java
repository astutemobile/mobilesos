package com.astutesolutions.mobilesos;

/**
 * Created by Rob Pungello on 9/23/16.
 * Copyright © 2016 Astute Solutions, Inc. All rights reserved.
 */

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.content.Context;


public class AMSConnectivityManager {

    private static AMSConnectivityManager instance = null;
    private ConnectivityManager cm = null;

    protected AMSConnectivityManager() {
        // Exists only to defeat instantiation.
    }

    public static AMSConnectivityManager sharedConnManager() {
        if(instance == null) {
            instance = new AMSConnectivityManager();
        }

        return instance;
    }

   public void setContext(Context context) {
       cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
   }

    public boolean isConnected() {
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return (activeNetwork != null) &&
                (activeNetwork.isConnectedOrConnecting());
    }

    public void showConnectionType() {
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        System.out.println("Connection type = " + activeNetwork.getTypeName());
    }

    public boolean isWiFiConnection() {
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
    }



}
