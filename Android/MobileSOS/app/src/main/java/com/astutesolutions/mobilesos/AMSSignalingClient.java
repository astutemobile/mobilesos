package com.astutesolutions.mobilesos;

/**
 * Created by Rob Pungello on 9/22/16.
 * Copyright © 2016 Astute Solutions, Inc. All rights reserved.
 */

import microsoft.aspnet.signalr.client.ConnectionState;
import microsoft.aspnet.signalr.client.StateChangedCallback;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;
import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.ErrorCallback;
import microsoft.aspnet.signalr.client.LogLevel;
import microsoft.aspnet.signalr.client.Logger;
import microsoft.aspnet.signalr.client.MessageReceivedHandler;
import microsoft.aspnet.signalr.client.SignalRFuture;
import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler1;
import microsoft.aspnet.signalr.client.transport.LongPollingTransport;
import microsoft.aspnet.signalr.client.transport.WebsocketTransport;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutionException;

import com.google.gson.JsonElement;

interface AMSSignalRListener {
    void connectionDidReceiveData(String data);
    void connectionDidOpen();
    void connectionDidFail();
    //void connectionStateChanged();
}

public class AMSSignalRClient {
    private List<AMSSignalRListener> listeners = new ArrayList<AMSSignalRListener>();
    private AMSConnectivityManager connectivityManager = AMSConnectivityManager.sharedConnManager();
    HubConnection conn = null;
    HubProxy proxy = null;
    Logger logger = null;
    boolean connected = false;

    private SignalRFuture<Void> awaitConnection;
    private SubscriptionHandler1 subscriptionHandler;




    public AMSSignalRClient() {
        this.configure();
    }

    // set up the callbacks
    private void configure() {
        System.out.println(">>>>> AMSSignalRClient - configuring the connection!!");

        // Create a new console logger
        logger = new Logger() {

            @Override
            public void log(String message, LogLevel level) {
                System.out.println(message);
            }
        };

        // Connect to the server
        String serverUrl = AMSConfiguration.sharedConfig().getInteractionBusUrl();
        conn = new HubConnection(serverUrl, "", true, logger);

        // Create the hub proxy
        proxy = conn.createHubProxy("InteractionBusHub");

        // test connectivity
        connectivityManager.showConnectionType();
        System.out.println(">>>>> connectivity info -> isConnected = " + connectivityManager.isConnected());
        System.out.println(">>>>> connectivity info -> isWifiConnection = " + connectivityManager.isWiFiConnection());


        // this doesn't get called!!
        proxy.subscribe(new Object() {
            @SuppressWarnings("unused")
            public void messageReceived(String name, String message) {
                System.out.println(">>>>> AMSSignalRClient messageReceived -> " + name + " : " + message);
            }
        });

        // Subscribe to the error event
        conn.error(new ErrorCallback() {

            @Override
            public void onError(Throwable error) {
                error.printStackTrace();
            }
        });

        // Subscribe to the connected event
        conn.connected(new Runnable() {
            @Override
            public void run() {
                connected = true;
                System.out.println(">>>>> AMSSignalRClient -> CONNECTED");
                getConnectionData();
            }
        });

        // Subscribe to the closed event
        conn.closed(new Runnable() {

            @Override
            public void run() {
                connected = false;
                System.out.println(">>>>> AMSSignalRClient -> DISCONNECTED");
            }
        });

        // Subscribe to the received event
        conn.received(new MessageReceivedHandler() {

            @Override
            public void onMessageReceived(JsonElement json) {
                System.out.println(">>>>> AMSSignalRClient -> RAW received message: " + json.toString());
                notifyConnectionDidReceiveData(json.toString());
            }
        });

        conn.reconnected(new Runnable() {
            @Override
            public void run() {
                connected = true;
                System.out.println(">>>>> AMSSignalRClient -> RECONNECTED");
            }
        });

        conn.reconnecting(new Runnable() {
            @Override
            public void run() {
                connected = true;
                System.out.println(">>>>> AMSSignalRClient -> RECONNECTING");
            }
        });

        conn.stateChanged(new StateChangedCallback() {
            @Override
            public void stateChanged(ConnectionState connectionState, ConnectionState connectionState1) {
                System.out.println(">>>>> AMSSignalRClient -> state changed from " + connectionState + " to " + connectionState1);
                getConnectionState();
            }
        });

        // Start the connection
        /*
        conn.start().done(new Action<Void>() {
            @Override
            public void run(Void obj) throws Exception {
                System.out.println("Done Connecting!");
            }
        });
        */

        awaitConnection = conn.start(new LongPollingTransport(conn.getLogger()));
        //awaitConnection = conn.start(new WebsocketTransport(conn.getLogger()));
        //you can do here a while loop for reintents or something ( this is a good practice, at least 3 tries.
        try {
            awaitConnection.get(2000, TimeUnit.MILLISECONDS);
            connected = true;
            startHandlerConnection();
            System.out.println(">>>>> CONNECTED <<<<<");

        } catch (InterruptedException e) {
            System.out.println("Disconnect . . .");
        } catch (ExecutionException e) {
            System.out.println("Error . . .");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startHandlerConnection(){
        subscriptionHandler = new SubscriptionHandler1<String>() {
            @Override
            public void run(String resp) {
                System.out.println(">>>>> message from server -> " + resp);
            }
        };

        proxy.on("SendToServer", subscriptionHandler, String.class);
    }


    public void startSignaling(String login) {
        proxy.invoke("SendToServer", login).done(new Action<Void>() {
            @Override
            public void run(Void obj) throws Exception {
                System.out.println(">>>>> AMSSignalRClient login sent!!");
            }
        });
    }

    public void stopSignaling(String logout) {
        proxy.invoke("SendToServer", logout).done(new Action<Void>() {
            @Override
            public void run(Void obj) throws Exception {
                System.out.println(">>>>> AMSSignalRClient logout sent!!");
            }
        });

    }

    public void disconnect(String disconn) {
        proxy.invoke("SendToServer", disconn).done(new Action<Void>() {
            @Override
            public void run(Void obj) throws Exception {
                System.out.println(">>>>> AMSSignalRClient disconnect sent!!");
                conn.stop();
            }
        });
    }

    public void getConnectionState() {
        ConnectionState state = conn.getState();
        System.out.println("Connection state = " + state.toString());
    }

    public void getConnectionData() {
        System.out.println("ConnectionData = " + conn.getConnectionData());

    }

    @SuppressWarnings("unused")
    public void addListener(AMSSignalRListener listener) {
        listeners.add(listener);
    }

    public void notifyConnectionDidReceiveData(String data) {
        for(AMSSignalRListener listener : listeners) {
            listener.connectionDidReceiveData(data);
        }
    }

    void notifyConnectionDidOpen() {
        for(AMSSignalRListener listener : listeners) {
            listener.connectionDidOpen();
        }
    }

    void notifyConnectionDidFail() {
        for(AMSSignalRListener listener : listeners) {
            listener.connectionDidFail();
        }
    }

    /*
    void notifyConnectionStateChanged() {
        for(AMSSignalRListener listener : listeners) {
            listener.connectionStateChanged();
        }
    }
    */
}