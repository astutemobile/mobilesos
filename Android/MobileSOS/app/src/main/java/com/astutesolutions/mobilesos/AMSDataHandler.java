package com.astutesolutions.mobilesos;

/**
 * Created by Rob Pungello on 9/20/16.
 * Copyright © 2016 Astute Solutions, Inc. All rights reserved.
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;

interface AMSDataHandlerListener {
    void didReceivePCIUserData(boolean allowAudio, boolean allowVideo);
    void didReceiveInteractionID(String interactionID);
    void setStunTurnServers(String turnUrl, String stunUrl, String username, String credentials);
    void didReceiveIceCandidate(String mid, int index, String sdp);
    void didReceiveRemoteSdp(String sdp, String offerType);
    void didReceiveDisconnect();
    void didReceiveLogout();
}

public class AMSDataHandler {
    private List<AMSDataHandlerListener> listeners = new ArrayList<AMSDataHandlerListener>();
    private AMSCasePopData casePopData;
    int candidateCount;

    public AMSDataHandler() {
        casePopData = new AMSCasePopData();
        candidateCount = 0;
    }

    public String serialize(HashMap map) {
        Gson gson = new GsonBuilder().create();
        //Type jsonStr = new TypeToken<String>(){}.getType();
        return gson.toJson(map);
    }


    public Map deserialize(String json) {
            Gson gson = new Gson();
            LinkedTreeMap result = gson.fromJson(json , LinkedTreeMap.class);
            return result;
    }

    /*
     *  SignalR messages
     */
    public LinkedHashMap createPCILoginRequest() {
        LinkedHashMap pciLogin = new LinkedHashMap<String,String>();
        pciLogin.put("_$ProviderList", "WebRTCProvider");
        pciLogin.put("_$UserName", "CUSTOMER");

        LinkedHashMap request = new LinkedHashMap();
        request.put("PCILogin", pciLogin);
        return request;
    }

    public LinkedHashMap createPCILogoutRequest() {
        LinkedHashMap pciLogout = new LinkedHashMap();
        pciLogout.put("_$Version", "2.0");

        LinkedHashMap request = new LinkedHashMap();
        request.put("PCILogout", pciLogout);
        return request;
    }

     public LinkedHashMap createPCIDisconnectRequest(String interactionID) {
         LinkedHashMap pciDisconnect = new LinkedHashMap();
         pciDisconnect.put("_$Version", "2.0");
         pciDisconnect.put("_$InteractionID", interactionID);

         LinkedHashMap request = new LinkedHashMap();
         request.put("PCIDisconnect", pciDisconnect);
         return request;
    }

    public String getPCILoginRequest() {
        return this.serialize(this.createPCILoginRequest());
    }

    public String getPCILogoutRequest() {
        return this.serialize(this.createPCILogoutRequest());
    }

    public String getPCIDisconnectRequest(String interactionID) {
        return this.serialize(this.createPCIDisconnectRequest(interactionID));
    }

    public LinkedHashMap getCasePopData() {
        return this.casePopData.getCasePopData();
    }

    public LinkedHashMap getCasePopData(String caller) {
        this.casePopData.addUserEnteredName(caller);
        return this.casePopData.getCasePopData();
    }

    public void setCasePopData(AMSCasePopData data) {
        casePopData = data;
    }

    /*
    "{"H":"InteractionBusHub","M":"SocketOnDataArrival","A":["{\"WebRTCiceServers\":{\"_$InteractionID\":\"\",\"_$ice\":\"{\\\"iceServers\\\": [{\\\"url\\\": \\\"stun:54.175.33.28:19302\\\"},{\\\"url\\\": \\\"turn:54.175.33.28:3478?transport=udp\\\", \\\"username\\\":\\\"1474897242:1426074175:harfry\\\", \\\"credential\\\":\\\"5NouDATRU++sB1BwuQrdQoczIfk=\\\"}]}\"}}"]}"
     "{"H":"InteractionBusHub","M":"SocketOnDataArrival","A":["{\"PCIUser\":{\"_$Version\":\"\",\"_$Status\":\"Available\",\"_$IsStatusAvailable\":\"Y\",\"_$HeartbeatResponse\":\"N\",\"_$WebRTCAudio\":\"Y\",\"_$WebRTCVideo\":\"Y\",\"PCIStatusList\":{\"PCIStatus\":[{\"_$Status\":\"Available\",\"_$HasUntilDate\":\"N\",\"_$HasUntilTime\":\"N\",\"_$IsUserSelectable\":\"Y\",\"_$IsStatusDND\":\"False\"},{\"_$Status\":\"Do Not disturb\",\"_$HasUntilDate\":\"N\",\"_$HasUntilTime\":\"N\",\"_$IsUserSelectable\":\"Y\",\"_$IsStatusDND\":\"True\"}]}}}"]}"
     */

    public void parseData(String jsonData) {
        Map dataMap = deserialize(jsonData);

        if(!dataMap.containsKey("A")) {
            return;
        }

        ArrayList a = (ArrayList)dataMap.get("A");
        Map data = deserialize((String) a.get(0));

        if(data.containsKey("PCIUser")) {
            Map info = (Map) data.get("PCIUser");
            boolean allowAudio = info.get("_$WebRTCAudio").equals("Y");
            boolean allowVideo = info.get("_$WebRTCVideo").equals("Y");
            System.out.println("allowAudio = " + allowAudio + ", allowVideo = " + allowVideo);
            notifyDidReceivePCIUserData(allowAudio, allowVideo);
        } else if(data.containsKey("WebRTCiceServers")) {
            String turnServer = null;
            String stunServer = null;
            String username = null;
            String creds = null;
            Map info = (Map) data.get("WebRTCiceServers");
            String ice = (String) info.get("_$ice");
            Map servers = deserialize(ice);
            ArrayList serverUrls = (ArrayList) servers.get("iceServers");
            Iterator i = serverUrls.iterator();
            while(i.hasNext()) {
                Map url = (Map) i.next();
                if(url.containsKey("username")) {
                    turnServer = (String) url.get("url");
                    username = (String) url.get("username");
                    creds = (String) url.get("credential");
                } else {
                    stunServer = (String) url.get("url");
                }
            }

            notifySetStunTurnServers(turnServer, stunServer, username, creds);
        } else if(data.containsKey("PCILogout")) {
            notifyDidReceiveLogout();
        }
        /*} else if([data objectForKey: @"PCIInteraction"] != nil){
            NSDictionary * pci =[data objectForKey:@ "PCIInteraction"];
            NSString * interactionID =[pci objectForKey:@ "_$InteractionID"];
            NSString * state =[pci objectForKey:@ "_$State"];

            if ([state intValue]==6){
                NSLog( @ "Call has been disconnected!");
                [delegate didReceiveDisconnect];
            }

            [delegate didReceiveInteractionID:interactionID];
        }

        /*
        } else if([data objectForKey: @"WebRTCsdp"] != nil) {
            NSDictionary *webrtc = [data objectForKey: @"WebRTCsdp"];
            NSString *interactionID = [webrtc objectForKey: @"_$InteractionID"];
            NSString *sdpStr = [webrtc objectForKey: @"_$sdp"];
            NSDictionary *sdp = [self deserialize: sdpStr];

            [delegate didReceiveInteractionID:interactionID];

            if([sdp objectForKey: @"candidate"] != nil) {
                candidateCount++;
                if([AMSConfiguration sharedConfig].debugMode) {
                    NSLog(@">>>>> AMSDataHandler received ICE candidate %i <<<<<", candidateCount);
                }

                NSString *candidate = [sdp objectForKey: @"candidate" ];
                [delegate didReceiveIceCandidateWithMid: [sdp objectForKey: @"sdpMid"]
                index: [[sdp objectForKey: @"sdpMLineIndex"] intValue]
                sdp: candidate];
            } else if([sdp objectForKey: @"type"] != nil) {
                NSString *type = [sdp objectForKey: @"type"];
                if([type isEqualToString: @"answer"]) {
                    [delegate didReceiveRemoteSdp: [sdp objectForKey: @"sdp"] ofType: @"answer"];
                } else if([type isEqualToString: @"offer"]) {
                    [delegate didReceiveRemoteSdp: [sdp objectForKey: @"sdp"] ofType: @"offer"];
                }
            }
        }
        else if([data objectForKey: @"PCILogout"] != nil) {
            [delegate didReceiveLogout];
        }
        else {
            NSLog(@">>>>> AMSDataHandler unrecognized data received from Interaction Bus!");
        }
        */
    }
    /*

    - (NSDictionary *) createWebRTCOffer: (RTCSessionDescription *) sessionDescripton forCaller: (NSString *) caller {
        NSDictionary *sdp = @{@"sdp" : sessionDescripton.description,
            @"type" : @"offer"};

        NSDictionary *inner = @{@"_$sdp" : [sdp SRJSONRepresentation],
            @"_$UserData" : [self serialize: @{@"CaseData" : caller == nil ? [self getCasePopData] : [self getCasePopData: caller]}]};

        NSDictionary *offer = @{@"WebRTCOffer" : inner};

        return offer;
    }

    - (NSDictionary *) createWebRTCIceCandidateRequest: (RTCICECandidate *) iceCandidate interactionID: (NSString *) interactionID {
        NSDictionary *sdp = @{@"_$sdp" : [[iceCandidate JSONDictionary] SRJSONRepresentation],
            @"_$InteractionID" : interactionID};

        NSDictionary *candidate = @{@"WebRTCsdp" : sdp};

        return candidate;
    }


    - (NSString *) getWebRTCIceCandidateRequest: (RTCICECandidate *) iceCandidate interactionID: (NSString *) interactionID {
        return [self serialize: [self createWebRTCIceCandidateRequest: iceCandidate interactionID:interactionID]];
    }

    - (NSString *) getWebRTCOffer: (RTCSessionDescription *) sdp forCaller: (NSString *) caller {
        return [self serialize: [self createWebRTCOffer: sdp forCaller: caller]];
    }




    - (NSString *) getWebRtcIceCandidateRequest: (NSString *) iceCandidate withInteractionID: (NSString *) interactionID {
        return [NSString stringWithFormat: @"{ 'WebRTCsdp': { '_$sdp': '%@', '_$InteractionID': '%@' } }",  iceCandidate, interactionID];
    }



    #pragma mark -  Data
    - (NSDictionary *) getCasePopData {
        return [casePopData getCasePopData];
    }

    - (NSDictionary *) getCasePopData: (NSString *) caller {
        [casePopData addUserEnteredName: caller];
        return [casePopData getCasePopData];
    }

    - (void) setCasePopData: (AMSCasePopData *) data {
        casePopData = data;
    }




    #pragma mark - Helpers
    - (void) printJsonDictionary: (NSDictionary *) jsonDict {
        NSEnumerator *enumerator = [jsonDict keyEnumerator];
        id key;
        while ((key = [enumerator nextObject])) {
            NSDictionary *tmp = [jsonDict objectForKey: key];
            NSLog(@"%@ = %@", key, tmp);
        }
    }


*/


    // AMSDataHandlerListener methods
    @SuppressWarnings("unused")
    public void addListener(AMSDataHandlerListener listener) {
        listeners.add(listener);
    }

    void notifyDidReceivePCIUserData(boolean allowAudio, boolean allowVideo) {
        for(AMSDataHandlerListener listener : listeners) {
            listener.didReceivePCIUserData(allowAudio, allowVideo);
        }
    }
    void notifyDidReceiveInteractionID(String interactionID) {
        for(AMSDataHandlerListener listener : listeners) {
            listener.didReceiveInteractionID(interactionID);
        }
    }

    void notifySetStunTurnServers(String turnUrl, String stunUrl, String username, String credentials) {
        for(AMSDataHandlerListener listener : listeners) {
            listener.setStunTurnServers(turnUrl, stunUrl, username, credentials);
        }
    }

    void notifyDidReceiveIceCandidate(String mid, int index, String sdp) {
        for(AMSDataHandlerListener listener : listeners) {
            listener.didReceiveIceCandidate(mid, index, sdp);
        }
    }

    void notifyDidReceiveRemoteSdp(String sdp, String offerType) {
        for(AMSDataHandlerListener listener : listeners) {
            listener.didReceiveRemoteSdp(sdp, offerType);
        }
    }

    void notifyDidReceiveDisconnect() {
        for(AMSDataHandlerListener listener : listeners) {
            listener.didReceiveDisconnect();
        }
    }

    void notifyDidReceiveLogout() {
        for(AMSDataHandlerListener listener : listeners) {
            listener.didReceiveLogout();
        }
    }

}
