package com.astutesolutions.mobilesos;

/**
 * Created by Rob Pungello on 9/19/16.
 * Copyright © 2016 Astute Solutions, Inc. All rights reserved.
 */

public class AMSMessage {
    public String userName;
    public String message;
}
