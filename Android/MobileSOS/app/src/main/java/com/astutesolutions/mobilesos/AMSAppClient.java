package com.astutesolutions.mobilesos;

/**
 * Created by Rob Pungello on 9/22/16.
 * Copyright © 2016 Astute Solutions, Inc. All rights reserved.
 */


//import com.ericsson.research.owr.sdk.RtcConfig;
//import com.ericsson.research.owr.sdk.RtcConfigs;

import org.webrtc.AudioSource;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.VideoTrack;
import org.webrtc.AudioTrack;
import org.webrtc.DataChannel;
import org.webrtc.MediaStream;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaConstraints.KeyValuePair;
import org.webrtc.PeerConnection.IceServer;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

enum AMSCallType {
    CHAT(0), AUDIO(1), VIDEO(2);
    private int value;


    AMSCallType(int i) {
        this.value = i;
    }
}

enum AMSState {
    CONNECTED(0), DISCONNECTED(1), CONNECTING(2);
    private int value;

    AMSState(int i) {this .value = i; }
}

public class AMSAppClient implements AMSSignalingClientListener, AMSDataHandlerListener, SdpObserver, PeerConnection.Observer, DataChannel.Observer {
    private static AMSAppClient instance = null;
    private static String VIDEO_TRACK_ID = "amsVideo0";
    private static String AUDIO_TRACK_ID = "amsAudio0";
    private static String LOCAL_STREAM_ID = "amsLocal0";


    private AMSSignalingClient signalRClient;
    private AMSDataHandler dataHandler;
    private AMSConfiguration config;


    PeerConnectionFactory factory;
    PeerConnection peerConnection;
    DataChannel dataChannel;
    VideoTrack defaultVideoTrack;
    AudioTrack defaultAudioTrack;

    ArrayList messageQueue;
    ArrayList<PeerConnection.IceServer> iceServers;
    String interactionID;
    String caller;
    boolean hasReceivedSdp;
    boolean hasReceivedServers;
    boolean hasSentOffer;
    boolean hasReceivedDisconnect;
    boolean allowAudio;
    boolean allowVideo;
    boolean wantsAudio;
    boolean wantsVideo;
    AMSCallType callType;
    AMSState state;


    boolean connectonError;
    String connectionErrorMsg;


    protected AMSAppClient() {
        // Exists only to defeat instantiation.
    }

    public static AMSAppClient sharedClient() {
        if(instance == null) {
            instance = new AMSAppClient();
            instance.configure();
        }

        return instance;
    }

    protected void configure() {
        config = AMSConfiguration.sharedConfig();

        signalRClient = new AMSSignalingClient();
        signalRClient.addListener(this);

        dataHandler = new AMSDataHandler();
        dataHandler.addListener(this);

        factory = new PeerConnectionFactory();

        messageQueue = new ArrayList();
        iceServers = new ArrayList<IceServer>();
        interactionID = null;
        caller = null;
        hasReceivedSdp = false;
        hasReceivedServers = false;
        hasSentOffer = false;
        hasReceivedDisconnect = false;
        allowAudio = true;
        allowVideo = true;
        wantsAudio = true;
        wantsVideo = true;
        callType = AMSCallType.VIDEO;
        connectonError = false;
        connectionErrorMsg = null;

    }

    // TODO should use sendToServer call here
    public void startSignaling() {
        signalRClient.startSignaling(dataHandler.getPCILoginRequest());
    }

    // TODO should use sendToServer call here
    public void stopSignaling() {

        signalRClient.stopSignaling(dataHandler.getPCILogoutRequest());
    }

    public void connect() {
        state = AMSState.CONNECTING;
        createPeerConnection();
    }

    private void cleanup() {
        signalRClient.sendToServer(dataHandler.getPCIDisconnectRequest(interactionID));
    }

    // should save state here..
    private void disconnect() {
        if(state == AMSState.DISCONNECTED) {
            return;
        }

        messageQueue.clear();
        peerConnection = null;
        signalRClient.sendToServer(dataHandler.getPCIDisconnectRequest(interactionID));
    }

    // TODO implement debug mode logging
    protected void sendChatMessage(String msg) {
        HashMap<String,String> map = new HashMap<>();
        map.put("message", msg);
        String jsonMessage = dataHandler.serialize(map);
        byte[] rawMessage = jsonMessage.getBytes(Charset.forName("UTF-8"));
        ByteBuffer dataBuffer = ByteBuffer.allocateDirect(jsonMessage.length());
        dataBuffer.put(rawMessage);
        DataChannel.Buffer messageBuffer = new DataChannel.Buffer(dataBuffer, false);
        if(!dataChannel.send(messageBuffer)) {
            System.out.println(">>>>> AMSAppClient: message failed to send over data channel");
        }
    }

    private void addTurnServer(String turnUrl) {
        PeerConnection.IceServer iceServer = new PeerConnection.IceServer(turnUrl);
        iceServers.add(iceServer);
    }

    private void addStunServer(String stunUrl, String username, String password) {
        PeerConnection.IceServer iceServer = new PeerConnection.IceServer(stunUrl, username, password);
        iceServers.add(iceServer);
    }

    protected void createPeerConnection() {
        if(!hasReceivedServers) {
            return;
        }

        state = AMSState.CONNECTED;
        MediaConstraints constraints = defaultPeerConnectionConstraints();

        peerConnection = factory.createPeerConnection(iceServers, constraints, this);

        MediaStream localStream = createLocalMediaStream();

    }

    protected MediaStream createLocalMediaStream() {
        MediaStream localStream = factory.createLocalMediaStream(LOCAL_STREAM_ID);
        VideoTrack localVideoTrack = createLocalVideoTrack();
        AudioTrack localAudioTrack = createLocalAudioTrack();


        if (localVideoTrack) {
            [localStream addVideoTrack: localVideoTrack];
            [_delegate clientDidReceiveLocalVideoTrack: localVideoTrack];
        }

        if(localAudioTrack) {
            [localStream addAudioTrack: [self createLocalAudioTrack]];
        }


        return localStream;
    }

    protected AudioTrack createLocalAudioTrack() {
        AudioSource audioSource = factory.createAudioSource(defaultMediaStreamConstraints());
        AudioTrack localAudioTrack = factory.createAudioTrack(AUDIO_TRACK_ID, audioSource);

        if(!wantsAudio) {
            localAudioTrack.setEnabled(false);
        }

        return localAudioTrack;
    }

   protected VideoTrack createLocalVideoTrack() {
        VideoTrack localVideoTrack = null;
        if (allowVideo) {
            MediaConstraints *mediaConstraints = defaultMediaStreamConstraints();
            VideoSource *source = factory.createVideoSource();
           // [[RTCAVFoundationVideoSource alloc] initWithFactory:_factory
           // constraints:mediaConstraints];

            //localVideoTrack =
            //[[RTCVideoTrack alloc] initWithFactory:_factory
            //source:source
            //trackId: VIDEO_TRACK_ID];
        }

        if(!wantsVideo) {
            //[localVideoTrack setEnabled: NO];
        } else {
            //[localVideoTrack setEnabled: YES];
        }

        return localVideoTrack;
    }





    // TODO in iOS we set mandatory and optional to nil..
    private MediaConstraints defaultMediaStreamConstraints() {
        MediaConstraints constraints = new MediaConstraints();
        return constraints;
    }

    private MediaConstraints defaultAnswerConstraints() {
        return defaultOfferConstraints();
    }

    private MediaConstraints defaultOfferConstraints() {
        List<MediaConstraints.KeyValuePair> mandatory = new ArrayList<>();
        mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveAudio", allowAudio ? "true" : "false"));
        mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveVideo", allowVideo ? "true" : "false"));

        MediaConstraints constraints = new MediaConstraints();
        constraints.mandatory.addAll(mandatory);

        return constraints;
    }

    private MediaConstraints defaultPeerConnectionConstraints() {
        List<MediaConstraints.KeyValuePair> mandatory = new ArrayList<>();
        mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveAudio", allowAudio ? "true" : "false"));
        mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveVideo", allowVideo ? "true" : "false"));
        mandatory.add(new MediaConstraints.KeyValuePair("minWidth", "200"));
        mandatory.add(new MediaConstraints.KeyValuePair("maxWidth", "400"));
        mandatory.add(new MediaConstraints.KeyValuePair("minHeight", "200"));
        mandatory.add(new MediaConstraints.KeyValuePair("maxHeight", "600"));

        List<MediaConstraints.KeyValuePair> optional = new ArrayList<>();
        optional.add(new MediaConstraints.KeyValuePair("DtlsSrtpKeyAgreement", "true"));
        optional.add(new MediaConstraints.KeyValuePair("internalSctpDataChannels", "true"));


        MediaConstraints constraints = new MediaConstraints();
        constraints.mandatory.addAll(mandatory);
        constraints.optional.addAll(optional);

        return constraints;
    }

    // AMSSignalingClientListener methods
    @Override
    public void connectionDidReceiveData(String data) {
        System.out.println(">>>>> signalR connection did receive data -> " + data);
        dataHandler.parseData(data);
    }

    @Override
    public void connectionDidOpen() {
        System.out.println(">>>>> signalR connection did open");
    }

    @Override
    public void connectionDidFail() {

        System.out.println(">>>>> signalR connection did fail");
    }

    // AMSDataHandlerListener methods
    @Override
    public void didReceivePCIUserData(boolean allowAudio, boolean allowVideo) {
        System.out.println(">>>>> didReceivePCIUserData -> allowAudio = " + allowAudio + ", allowVideo = " + allowVideo);

    }

    @Override
    public void didReceiveInteractionID(String interactionID) {
        System.out.println(">>>>> didReceiveInteractionID = " + interactionID);
        this.interactionID = interactionID;
    }

    @Override
    public void setStunTurnServers(String turnUrl, String stunUrl, String username, String password) {
        System.out.println(">>>>> setStunTurnServers -> turn = " + turnUrl + ", stun = " + stunUrl + ", username = " + username + ", creds = " + credentials);
        addTurnServer(turnUrl);
        addStunServer(stunUrl, username, password);
    }

    @Override
    public void didReceiveIceCandidate(String mid, int index, String sdp) {
        System.out.println(">>>>> didReceiveIceCandidate -> mid = " + mid + ", sdp = " + sdp);
    }

    @Override
    public void didReceiveRemoteSdp(String sdp, String offerType) {
        System.out.println(">>>>> didReceiveRemoteSdp -> sdp = " + sdp + ", offerType = " + offerType);
    }

    @Override
    public void didReceiveDisconnect() {
        System.out.println(">>>>> AMSAppclient:didReceiveDisconnect!!!");
    }

    @Override
    public void didReceiveLogout() {

        System.out.println(">>>>> AMSAppClient:dReceiveLogout!!!");
    }


    // PeerConnection.Observer methods

    /** Triggered when the SignalingState changes. */
    @Override
    public void onSignalingChange(PeerConnection.SignalingState signalingState) {
        System.out.println(">>>>> AMSAppClient:onSignalingStateChange -> " + signalingState.toString());

    }

    /** Triggered when the IceConnectionState changes. */
    @Override
    public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
        System.out.println(">>>>> AMSAppClient:onIceConnectionChange -> " + iceConnectionState.toString());

    }

    /** Triggered when the IceGatheringState changes. */
    @Override
    public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {
        System.out.println(">>>>> AMSAppClient:onIceGatheringStateChange -> " + iceGatheringState.toString());


    }

    /** Triggered when a new ICE candidate has been found. */
    @Override
    public void onIceCandidate(IceCandidate iceCandidate) {
        System.out.println(">>>>> AMSAppClient:onIceCandidate -> " + iceCandidate.toString());


    }

    /** Triggered on any error. */
    @Override
    public void onError() {
        System.out.println(">>>>> AMSAppClient:onError called!!!!");


    }

    /** Triggered when media is received on a new stream from remote peer. */
    @Override
    public void onAddStream(MediaStream mediaStream) {
        System.out.println(">>>>> AMSAppClient:onAddStream -> " + mediaStream.toString());


    }

    /** Triggered when a remote peer close a stream. */
    @Override
    public void onRemoveStream(MediaStream mediaStream) {
        System.out.println(">>>>> AMSAppClient:onRemoveStream -> " + mediaStream.toString());


    }

    /** Triggered when a remote peer opens a DataChannel. */
    @Override
    public void onDataChannel(DataChannel dataChannel) {
        System.out.println(">>>>> AMSAppClient:onDataChannel -> " + dataChannel.toString());

    }

    /** Triggered when renegotiation is necessary. */
    @Override
    public void onRenegotiationNeeded() {
        System.out.println(">>>>> AMSAppClient:onRenegotiationNeeded called!!!");


    }


    // SdpObserver methods

    /** Called on success of Create{Offer,Answer}(). */
    @Override
    public void onCreateSuccess(SessionDescription sessionDescription) {
        System.out.println(">>>>> AMSAppClient:onCreateSuccess -> " + sessionDescription.toString());


    }

    /** Called on success of Set{Local,Remote}Description(). */
    @Override
    public void onSetSuccess() {
        System.out.println(">>>>> AMSAppClient:onSetSuccess!!!");
    }

    /** Called on error of Create{Offer,Answer}(). */
    @Override
    public void onCreateFailure(String s) {
        System.out.println(">>>>> AMSAppClient:onCreateFailure -> " + s);


    }

    /** Called on error of Set{Local,Remote}Description(). */
    @Override
    public void onSetFailure(String s) {
        System.out.println(">>>>> AMSAppClient:onSetFailure -> " + s);

    }

    // DataChannel.Observer methods
    @Override
    public void onStateChange() {
        System.out.println(">>>>> AMSAppClient DataChannel:onStateChange");

    }

    @Override
    public void onMessage(DataChannel.Buffer buffer) {
        System.out.println(">>>>> AMSAppClient: DataChannel:onMessage -> " + buffer.toString());

    }

}